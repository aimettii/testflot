## Backend:

- cd to root directory
- sudo chown -R $USER:$USER .
- cp .env.example .env
- docker run --rm -v $(pwd):/app composer require laravel/sail --dev
- alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
- sail up -d
- sail php artisan key:generate
- sail php artisan migrate
- sail php artisan storage:link

go to http://localhost

Могут возникнуть проблемы доступа к базе если разворачивать через sail, в основном там обычные зависимости

Нужно проверить http://localhost/api/advertisements. Если проблема с доступом к базе, то можно попататься собрать проект дефолтно, через вагрант или просто свой настроенный докер, обновив все composer зависимости и вызвав команды:
- php artisan key:generate
- php artisan migrate
- php artisan storage:link

Laravel: 9 версия, PHP: 8


## Frontend:

- cd ./front
    Настроить API ендпоинт URL в env, cp .env.example .env (по дефолту http://localhost/api)
- npm install
- npm run dev
go to http://localhost:3000
