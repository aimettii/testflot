// This store resolve bug with auth module
export const state = () => ({
  loggedIn: false,
  user: null
})

export const mutations = {
  loggedIn(state, bool) {
    state.loggedIn = bool;
  },
  user(state, user) {
    state.user = user;
  }
}
