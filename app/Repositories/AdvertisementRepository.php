<?php

namespace App\Repositories;

use App\Models\Advertisement;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AdvertisementRepository extends BaseRepository
{
    protected $fieldSearchable = [

    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Advertisement::class;
    }

    public function create(array $input): Model
    {
        $model = $this->model->newInstance($input);

        $model->author()->associate(Auth::user());

        $model->save();

        return $model;
    }
}
