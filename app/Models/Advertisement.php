<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    public $table = 'advertisements';

    public $fillable = [
        'name',
        'photo',
        'author_id'
    ];

    protected $casts = [
        'name' => 'string',
        'photo' => 'string'
    ];

    public static $rules = [
        'name' => 'required|min:1',
        'photo' => 'required'
    ];

    protected $appends = ['photo_url'];

    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'author_id', 'id');
    }

    public function getPhotoUrlAttribute()
    {
        return asset('storage/' . $this->photo);
    }
}
