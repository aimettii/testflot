<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAdvertisementAPIRequest;
use App\Http\Requests\API\UpdateAdvertisementAPIRequest;
use App\Http\Resources\HomeAdvertisementResource;
use App\Models\Advertisement;
use App\Repositories\AdvertisementRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class AdvertisementAPIController
 */
class AdvertisementAPIController extends AppBaseController
{
    private AdvertisementRepository $advertisementRepository;

    public function __construct(AdvertisementRepository $advertisementRepo)
    {
        $this->advertisementRepository = $advertisementRepo;
    }

    /**
     * Display a listing of the Advertisements.
     * GET|HEAD /advertisements
     */
    public function index(Request $request): JsonResponse
    {
        $this->sendResponse(HomeAdvertisementResource::collection([]), 'Advertisements retrieved successfully');
        $advertisements = $this->advertisementRepository->paginate(
            10
        );

        return $this->sendResponse(HomeAdvertisementResource::collection($advertisements), 'Advertisements retrieved successfully');
    }

    /**
     * Store a newly created Advertisement in storage.
     * POST /advertisements
     */
    public function store(CreateAdvertisementAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $photoName = sprintf('%s.%s', uniqid(), $request->photo->extension());

        $request->photo->storeAs('public', $photoName);

        $input['photo'] = $photoName;
        $advertisement = $this->advertisementRepository->create($input);

        return $this->sendResponse($advertisement->toArray(), 'Advertisement saved successfully');
    }

    /**
     * Display the specified Advertisement.
     * GET|HEAD /advertisements/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var  Advertisement $advertisement */
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        return $this->sendResponse($advertisement->toArray(), 'Advertisement retrieved successfully');
    }

    /**
     * Update the specified Advertisement in storage.
     * PUT/PATCH /advertisements/{id}
     */
    public function update($id, UpdateAdvertisementAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var  Advertisement $advertisement */
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        $advertisement = $this->advertisementRepository->update($input, $id);

        return $this->sendResponse($advertisement->toArray(), 'Advertisement updated successfully');
    }

    /**
     * Remove the specified Advertisement from storage.
     * DELETE /advertisements/{id}
     *
     * @throws  \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var  Advertisement $advertisement */
        $advertisement = $this->advertisementRepository->find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        $advertisement->delete();

        return $this->sendSuccess('Advertisement deleted successfully');
    }
}
