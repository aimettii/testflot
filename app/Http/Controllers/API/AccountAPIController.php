<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Resources\AccountResource;
use Illuminate\Http\Request;

class AccountAPIController extends AppBaseController
{
    public function __invoke(Request $request) {
        return $this->sendResponse(new AccountResource($request->user()), 'Account retrieved successfully');
    }
}
