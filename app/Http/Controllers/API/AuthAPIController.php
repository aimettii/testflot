<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthAPIController extends AppBaseController
{
    public function token(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return $this->sendErrors('Validation errors', $validator->errors()->all(), 401);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return $this->sendError('The provided credentials are incorrect', 401);
        }

        return $this->sendResponse(['token' => $user->createToken($request->email)->plainTextToken], 'Authorization successful, API token was issued');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255',],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return $this->sendErrors('Validation errors', $validator->errors()->all(), 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $token = $user->createToken($request->email)->plainTextToken;

        return $this->sendResponse(['token' => $token], 'Authorization successful, API token was issued');
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        return $this->sendSuccess('Cleaning the authentication information was successful');
    }

}
