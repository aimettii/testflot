<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use InfyOm\Generator\Utils\ResponseUtil;

/**
 * @OA\Server(url="/api")
 * @OA\Info(
 *   title="InfyOm Laravel Generator APIs",
 *   version="1.0.0"
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        $arrResponse = ResponseUtil::makeResponse($message, $result);

        if ($result instanceof ResourceCollection && $result->resource instanceof LengthAwarePaginator) {
            $arrResponse = array_merge($arrResponse, (array) $result->toResponse(new Request())->getData());
        }

        return response()->json($arrResponse);
    }

    public function sendError($error, $code = 404)
    {
        return response()->json(ResponseUtil::makeError($error), $code);
    }

    public function sendErrors($error, $errors, $code = 404)
    {
        return response()->json(ResponseUtil::makeError($error, $errors), $code);
    }

    public function sendSuccess($message)
    {
        return response()->json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
