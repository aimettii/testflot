<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/token', 'AuthAPIController@token');
Route::post('auth/register', 'AuthAPIController@register');

Route::middleware('auth:sanctum')->group(function () {
    Route::get('auth/user', 'AccountAPIController');
    Route::post('auth/logout', 'AuthAPIController@logout');
    Route::resource('advertisements', App\Http\Controllers\API\AdvertisementAPIController::class)
        ->except(['create', 'edit', 'index']);
});

Route::get('advertisements', 'AdvertisementAPIController@index');
